const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const fs = require('fs');
const fxns = require('../graybar/parse-graybar-pdfs.js');

const expect = chai.expect;
chai.use(chaiAsPromised);

const generateQuoteLineItemsFromGraybarExport = fxns.generateLineItems;
const hasValidQuotedQuantities = fxns.checkQuantities;

const requestedLineItems1 = [
  {
    productSummary: {
      description: '1 1/4" LB die cast combo',
      partNumber: null,
      id: null,
    },
    quantityRequested: 500,
  },
  {
    productSummary: {
      description: '1-1/4" UNIVERSAL CLAMP (STRUT STRAP)',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 500,
  },
  {
    productSummary: {
      description: '8" x 6" x 4" jb no ko',
      partNumber: null,
      id: null,
    },
    quantityRequested: 100,
  },
  {
    productSummary: {
      description: '024K8P-31130-29',
      partNumber: null,
      id: null,
    },
    quantityRequested: 50,
  },
];

const requestedLineItems2 = [
  {
    productSummary: {
      description: '1 1/4" LB die cast combo',
      partNumber: null,
      id: null,
    },
    quantityRequested: 2,
  },
  {
    productSummary: {
      description: '1-1/4" UNIVERSAL CLAMP (STRUT STRAP)',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 25,
  },
  {
    productSummary: {
      description: '8" x 6" x 4" jb no ko',
      partNumber: null,
      id: null,
    },
    quantityRequested: 2,
  },
  {
    productSummary: {
      description: 'STEEL THINWALL CONDUIT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 2,
  },
  {
    productSummary: {
      description: 'SCH40 COND 10FT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 2,
  },
  {
    productSummary: {
      description: 'SCH40',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 25,
  },
  {
    productSummary: {
      description: 'ELL-90-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 2,
  },
  {
    productSummary: {
      description: 'SCH40 ELBOW',
      partNumber: null,
      id: null,
    },
    quantityRequested: 2,
  },
  {
    productSummary: {
      description: 'PVC-3/4',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 25,
  },
  {
    productSummary: {
      description: '3/4 EMT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 2,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 2,
  },
];
const correctAnswer1 = {
  comment:
    'THIS IS A HEADER NOTE AND  APPLIES TO THE ENTIRE QUOTE AS  A WHOLE. "ALL ITEMS STOCK IN ALBANY"',
  totalCostInCents: 37330,
  lineItems: [
    {
      productSummary: {
        description: '1 1/4" LB die cast combo',
        partNumber: null,
        id: null,
      },
      quantityRequested: 500,
      quantityAvailable: 500,
      quantityPerGroup: 500,
      pricePerGroupInCents: 24395,
      comment:
        'THIS IS THE LINE ITEM NOTE FOR LINE 100."SOME STOCK IN ALBANY SOME IN OHIO"',
    },
    {
      productSummary: {
        description: '1-1/4" UNIVERSAL CLAMP (STRUT STRAP)',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 500,
      quantityAvailable: 500,
      quantityPerGroup: 500,
      pricePerGroupInCents: 9670,
      comment: 'THIS IS THE LINE ITEM NOTE FOR LINE 200."ALL STOCK IN ALBANY"',
    },
    {
      productSummary: {
        description: '8" x 6" x 4" jb no ko',
        partNumber: null,
        id: null,
      },
      quantityRequested: 100,
      quantityAvailable: 100,
      quantityPerGroup: 100,
      pricePerGroupInCents: 1237,
      comment:
        'THIS IS THE LINE ITEM NOTE FOR LINE 300."2-3 DAYS TO SHIP PLUS UPS"',
    },
    {
      productSummary: {
        description: '024K8P-31130-29',
        partNumber: null,
        id: null,
      },
      quantityRequested: 50,
      quantityAvailable: 50,
      quantityPerGroup: 50,
      pricePerGroupInCents: 2028,
    },
  ],
};

const correctAnswer2 = {
  comment: '',
  totalCostInCents: 52650,
  lineItems: [
    {
      productSummary: {
        description: '1 1/4" LB die cast combo',
        partNumber: null,
        id: null,
      },
      quantityRequested: 2,
      quantityAvailable: 1,
      quantityPerGroup: 1,
      pricePerGroupInCents: 405,
      comment: 'Graybar stock in Texas.',
    },
    {
      productSummary: {
        description: '1-1/4" UNIVERSAL CLAMP (STRUT STRAP)',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 25,
      quantityAvailable: 1,
      quantityPerGroup: 1,
      pricePerGroupInCents: 211,
      comment: 'Graybar stock in Virginia.',
    },
    {
      productSummary: {
        description: '8" x 6" x 4" jb no ko',
        partNumber: null,
        id: null,
      },
      quantityRequested: 2,
      quantityAvailable: 1,
      quantityPerGroup: 1,
      pricePerGroupInCents: 1391,
      comment: 'Graybar stock in Albany.',
    },
    {
      productSummary: {
        description: 'STEEL THINWALL CONDUIT',
        partNumber: null,
        id: null,
      },
      quantityRequested: 2,
      quantityAvailable: 1,
      quantityPerGroup: 1,
      pricePerGroupInCents: 1761,
      comment: 'Graybar stock in New Jersey.',
    },
    {
      productSummary: {
        description: 'SCH40 COND 10FT',
        partNumber: null,
        id: null,
      },
      quantityRequested: 2,
      quantityAvailable: 1,
      quantityPerGroup: 1,
      pricePerGroupInCents: 29254,
      comment: 'Graybar stock in Albany.',
    },
    {
      productSummary: {
        description: 'SCH40',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 25,
      quantityAvailable: 1,
      quantityPerGroup: 1,
      pricePerGroupInCents: 1557,
      comment: 'Graybar stock in Albany.',
    },
    {
      productSummary: { description: 'ELL-90-3/4', partNumber: null, id: null },
      quantityRequested: 2,
      quantityAvailable: 1,
      quantityPerGroup: 1,
      pricePerGroupInCents: 7694,
      comment: 'Graybar stock in New Jersey.',
    },
    {
      productSummary: {
        description: 'SCH40 ELBOW',
        partNumber: null,
        id: null,
      },
      quantityRequested: 2,
      quantityAvailable: 1,
      quantityPerGroup: 1,
      pricePerGroupInCents: 8678,
      comment: 'Graybar stock in Albany.',
    },
    {
      productSummary: {
        description: 'PVC-3/4',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 25,
      quantityAvailable: 1,
      quantityPerGroup: 1,
      pricePerGroupInCents: 999,
      comment: 'Graybar stock in Albany.',
    },
    {
      productSummary: { description: '3/4 EMT', partNumber: null, id: null },
      quantityRequested: 2,
      quantityAvailable: 1,
      quantityPerGroup: 1,
      pricePerGroupInCents: 501,
      comment: 'Graybar stock in Albany.',
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 2,
      quantityAvailable: 1,
      quantityPerGroup: 1,
      pricePerGroupInCents: 199,
      comment: 'Graybar stock in New Jersey.',
    },
  ],
};

function calculateTotalCostInCentsForLineItems(lineItems) {
  let actualTotal = 0;
  lineItems.forEach((li) => {
    actualTotal += li.pricePerGroupInCents;
  });
  return actualTotal;
}

/* eslint-disable no-undef */
describe('Graybar PDF 1 with equal number of requested lines', () => {
  it('updated line items match answer 1', () => {
    const pdfReadStream1 = fs.createReadStream(
      './suppliers/graybar/graybar1.pdf',
    );
    const result = generateQuoteLineItemsFromGraybarExport(
      pdfReadStream1,
      requestedLineItems1,
    );
    return result.then((fulfilled) => {
      expect(fulfilled.comment).to.equal(correctAnswer1.comment);
      expect(
        calculateTotalCostInCentsForLineItems(fulfilled.lineItems),
      ).to.equal(correctAnswer1.totalCostInCents);
      fulfilled.lineItems.forEach((part, index) => {
        expect(part).to.deep.equal(correctAnswer1.lineItems[index]);
      });
    });
  });
});

describe('Graybar PDF 1 with  incorrect number of requested lines', () => {
  it('should error', () => {
    const pdfReadStream1 = fs.createReadStream(
      './suppliers/graybar/graybar1.pdf',
    );
    const result = generateQuoteLineItemsFromGraybarExport(
      pdfReadStream1,
      requestedLineItems2,
    );
    return expect(result).to.be.rejectedWith(
      'Incorrect Number of items requested',
    );
  });
});

describe('Graybar PDF 2 with equal number of requested lines', () => {
  it('updated line items match answer 2', () => {
    const pdfReadStream2 = fs.createReadStream(
      './suppliers/graybar/graybar2.pdf',
    );
    const result2 = generateQuoteLineItemsFromGraybarExport(
      pdfReadStream2,
      requestedLineItems2,
    );
    return result2.then((fulfilled) => {
      expect(fulfilled.comment).to.equal(correctAnswer2.comment);
      expect(
        calculateTotalCostInCentsForLineItems(fulfilled.lineItems),
      ).to.equal(correctAnswer2.totalCostInCents);
      fulfilled.lineItems.forEach((part, index) => {
        expect(part).to.deep.equal(correctAnswer2.lineItems[index]);
      });
    });
  });
});

const lineItems = {
  headerNote:
    'THIS IS A HEADER NOTE AND  APPLIES TO THE ENTIRE QUOTE AS  A WHOLE. "ALL ITEMS STOCK IN ALBANY"',
  lineItems: [
    {
      partNumber: '3/4-EMT',
      description: 'STEEL THINWALL CONDUIT',
      quantity: -500,
      extendedPrice: 24395,
      note:
        'THIS IS THE LINE ITEM NOTE FOR LINE 100."SOME STOCK IN ALBANY SOME IN OHIO"',
      unit: 100,
    },
    {
      partNumber: 'PVC-3/4',
      description: 'SCH40 COND 10FT',
      quantity: 500,
      extendedPrice: 9670,
      note: 'THIS IS THE LINE ITEM NOTE FOR LINE 200."ALL STOCK IN ALBANY"',
      unit: 100,
    },
    {
      partNumber: 'COUP-3/4',
      description: 'SCH40',
      quantity: 100,
      extendedPrice: 1237,
      note:
        'THIS IS THE LINE ITEM NOTE FOR LINE 300."2-3 DAYS TO SHIP PLUS UPS"',
      unit: 100,
    },
    {
      partNumber: 'ELL-90-3/4',
      description: 'SCH40 ELBOW',
      quantity: 50,
      extendedPrice: 2028,
      note: '',
      unit: 100,
    },
  ],
};

describe('hasValidQuotedQuantities()', () => {
  it('returns false if negative', () => {
    const result = hasValidQuotedQuantities(lineItems, requestedLineItems1);
    expect(result).to.equal(false);
  });
});

const lineItems2 = {
  headerNote:
    'THIS IS A HEADER NOTE AND  APPLIES TO THE ENTIRE QUOTE AS  A WHOLE. "ALL ITEMS STOCK IN ALBANY"',
  lineItems: [
    {
      partNumber: '3/4-EMT',
      description: 'STEEL THINWALL CONDUIT',
      quantity: 5000,
      extendedPrice: 24395,
      note:
        'THIS IS THE LINE ITEM NOTE FOR LINE 100."SOME STOCK IN ALBANY SOME IN OHIO"',
      unit: 100,
    },
    {
      partNumber: 'PVC-3/4',
      description: 'SCH40 COND 10FT',
      quantity: 500,
      extendedPrice: 9670,
      note: 'THIS IS THE LINE ITEM NOTE FOR LINE 200."ALL STOCK IN ALBANY"',
      unit: 100,
    },
    {
      partNumber: 'COUP-3/4',
      description: 'SCH40',
      quantity: 100,
      extendedPrice: 1237,
      note:
        'THIS IS THE LINE ITEM NOTE FOR LINE 300."2-3 DAYS TO SHIP PLUS UPS"',
      unit: 100,
    },
    {
      partNumber: 'ELL-90-3/4',
      description: 'SCH40 ELBOW',
      quantity: 50,
      extendedPrice: 2028,
      note: '',
      unit: 100,
    },
  ],
};
describe('hasValidQuotedQuantities()', () => {
  it('returns false if quantity from pdf is greater then initial request', () => {
    const result = hasValidQuotedQuantities(lineItems2, requestedLineItems1);
    expect(result).to.equal(false);
  });
});
