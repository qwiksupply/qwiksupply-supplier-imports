const pdfreader = require('pdfreader');
const fs = require('fs');
const tmp = require('tmp');

const PARTNUM_X = 33.859;
const DESCRIPTION_X = 51.888;
const EXTPRICE_X = 90;
const UNIT_X = 78.684;
const QUANTITY_X_LOWERBOUND = 12;
const QUANTITY_X_HIGHERBOUND = 14;
const FIRST_PART = '100';
const SKIP_LINES = 2;
const NUM_LAST_PRODUCT_LINES = 13;

function generateQuoteLineItemsFromGraybarExport(exportStream, requestedLineItems) {
  const tmpobj = tmp.fileSync();
  const pdfName = tmpobj.name;
  return new Promise((resolve, reject) => {
    const pdfWriteStream = fs.createWriteStream(pdfName);
    pdfWriteStream.on('close', () => {
      let pageContent = [];
      let lineItemDataFromPDF = { headerNote: '', lineItems: [] };
      new pdfreader.PdfReader().parseFileItems(pdfName, (err, item) => {
        const hasReachedEndOfPage = !item;
        if (hasReachedEndOfPage) {
          if (hasCorrectNumberOfLineItems(requestedLineItems, lineItemDataFromPDF)) {
            if (hasValidQuotedQuantities(lineItemDataFromPDF, requestedLineItems)) {
              const updatedLineItems =
              generateUpdatedLineItems(lineItemDataFromPDF, requestedLineItems);
              const headerItemObject =
              { comment: lineItemDataFromPDF.headerNote, lineItems: updatedLineItems };
              resolve(headerItemObject);
            } else {
              reject(new Error('Invalid Quantities'));
            }
          } else {
            reject(new Error('Incorrect Number of items requested'));
          }
        } else if (item.text) {
          pageContent.push(item);
        } else if (item.page) {
          lineItemDataFromPDF = getLineItemsFromPDF(lineItemDataFromPDF, pageContent);
          pageContent = [];
        }
      });
    });
    exportStream.pipe(pdfWriteStream);
  });
}

function hasValidQuotedQuantities(lineItemDataFromPDF, requestedLineItems) {
  let foundNeg = true;
  lineItemDataFromPDF.lineItems.forEach((item, index) => {
    if (item.quantity < 0 || item.quantity > requestedLineItems[index].quantityRequested) {
      foundNeg = false;
    }
  });
  return foundNeg;
}

function hasCorrectNumberOfLineItems(requestedLineItems, lineItemDataFromPDF) {
  return lineItemDataFromPDF.lineItems.length === requestedLineItems.length;
}

function getLineItemsFromPDF(lineItemDataFromPDF, pageContent) {
  const updatedPDFData = { headerNote: '', lineItems: [] };
  const productStartLineNums = getProductStartLineNums(pageContent);
  updatedPDFData.headerNote = getHeader(pageContent);
  updatedPDFData.lineItems = lineItemDataFromPDF.lineItems;
  productStartLineNums.forEach((startLineNumber, indexOfProduct) => {
    let nextPartStarts = productStartLineNums[indexOfProduct + 1];
    if (indexOfProduct === (productStartLineNums.length - 1)) {
      nextPartStarts = startLineNumber + NUM_LAST_PRODUCT_LINES;
    }
    const product = pageContent.slice(startLineNumber, nextPartStarts - 1);
    const productObj = getProductData(product);
    if (updatedPDFData.lineItems.every(onlyAppearsOnce, productObj)) {
      updatedPDFData.lineItems.push(productObj);
    }
  });
  return updatedPDFData;
}

function getHeader(pageContent) {
  let header = '';
  const indexOfHeaderStart = pageContent.findIndex(line => line.text.includes('Ext.Price')) + SKIP_LINES;
  const indexOfFirstPart = pageContent.findIndex(line => line.text.includes(FIRST_PART));
  const headerSlice = pageContent.slice(indexOfHeaderStart, indexOfFirstPart);
  const headerText = [];
  headerSlice.forEach(line => headerText.push(line.text));
  header = headerText.join(' ').replace('Notes: ', '');
  if (header.includes('EA ')) {
    header = '';
  }
  return header;
}

function getProductStartLineNums(pageContent) {
  const productStartLineNums = [];
  pageContent.forEach((line, index) => {
    if (line.text.includes('EA')) {
      if ((line.x) > QUANTITY_X_LOWERBOUND && (line.x) < QUANTITY_X_HIGHERBOUND) {
        productStartLineNums.push(index);
      }
    }
  });
  return productStartLineNums;
}

function getProductData(product) {
  let quantity = 0;
  let partNumber = '';
  let description = '';
  let extendedPrice = '';
  let comment = '';
  let unit = '';
  let noteStarted = false;
  product.forEach((line, indexOfLine) => {
    const lineText = line.text;
    const xcoord = line.x;
    if (indexOfLine === 0) {
      quantity = parseInt(lineText, 10);
    }
    if (xcoord === PARTNUM_X) {
      partNumber = `${partNumber}${lineText} `;
    }
    if (xcoord === DESCRIPTION_X) {
      description = `${description}${lineText} `;
    }
    if (xcoord > EXTPRICE_X) {
      extendedPrice = parseInt(lineText.replace('$', '').replace('.', ''), 10);
    }
    if (xcoord === UNIT_X) {
      unit = parseInt(lineText, 10);
    }
    if (lineText.includes('***Item Note:') || noteStarted) {
      comment += lineText.replace('***Item Note:***', '').replace(/_/g, '');
      noteStarted = true;
    }
  });
  partNumber = partNumber.trim();
  description = description.trim();
  return { partNumber, description, quantity, extendedPrice, comment, unit };
}

function onlyAppearsOnce(element) {
  return element.description !== this.description;
}

function generateUpdatedLineItems(lineItemDataFromPDF, requestedLineItems) {
  const updatedLineItems = requestedLineItems.map((requestedLineItem, index) => {
    const updatedLineItem = {
      productSummary: requestedLineItem.productSummary,
      quantityRequested: requestedLineItem.quantityRequested,
      quantityAvailable: lineItemDataFromPDF.lineItems[index].quantity,
      quantityPerGroup: lineItemDataFromPDF.lineItems[index].quantity,
      pricePerGroupInCents: lineItemDataFromPDF.lineItems[index].extendedPrice,
    };
    if (lineItemDataFromPDF.lineItems[index].comment !== '') {
      updatedLineItem.comment = lineItemDataFromPDF.lineItems[index].comment;
    }
    return updatedLineItem;
  });
  return updatedLineItems;
}

module.exports = { generateLineItems: generateQuoteLineItemsFromGraybarExport,
  checkQuantities: hasValidQuotedQuantities };
