const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const fs = require('fs');
const generateQuoteLineItemsFromHzExport = require('../hz/parse-hz-pdfs.js');

chai.use(chaiAsPromised);
const expect = chai.expect;

const requestedLineItems1 = [
  {
    productSummary: {
      description: '1 1/4" LB die cast combo',
      partNumber: null,
      id: null,
    },
    quantityRequested: 35,
  },
  {
    productSummary: {
      description: '1-1/4" UNIVERSAL CLAMP (STRUT STRAP)',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 20,
  },
  {
    productSummary: {
      description: '8" x 6" x 4" jb no ko',
      partNumber: null,
      id: null,
    },
    quantityRequested: 20,
  },
  {
    productSummary: {
      description: 'STEEL THINWALL CONDUIT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 15,
  },
  {
    productSummary: {
      description: 'SCH40 COND 10FT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 25,
  },
  {
    productSummary: {
      description: 'SCH40',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 5,
  },
  {
    productSummary: {
      description: 'ELL-90-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 10,
  },
  {
    productSummary: {
      description: 'SCH40 ELBOW',
      partNumber: null,
      id: null,
    },
    quantityRequested: 5,
  },
  {
    productSummary: {
      description: 'PVC-3/4',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 3,
  },
  {
    productSummary: {
      description: '3/4 EMT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 100,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 100,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 20,
  },
];
const requestedLineItems2 = [
  {
    productSummary: {
      description: '1 1/4" LB die cast combo',
      partNumber: null,
      id: null,
    },
    quantityRequested: 20000,
  },
  {
    productSummary: {
      description: '1-1/4" UNIVERSAL CLAMP (STRUT STRAP)',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 5000,
  },
  {
    productSummary: {
      description: '8" x 6" x 4" jb no ko',
      partNumber: null,
      id: null,
    },
    quantityRequested: 5000,
  },
  {
    productSummary: {
      description: 'STEEL THINWALL CONDUIT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 5000,
  },
  {
    productSummary: {
      description: 'SCH40 COND 10FT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 100,
  },
  {
    productSummary: {
      description: 'SCH40',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 200,
  },
  {
    productSummary: {
      description: 'ELL-90-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 50,
  },
  {
    productSummary: {
      description: 'SCH40 ELBOW',
      partNumber: null,
      id: null,
    },
    quantityRequested: 50,
  },
  {
    productSummary: {
      description: 'PVC-3/4',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 200,
  },
  {
    productSummary: {
      description: '3/4 EMT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 50,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 50,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 100,
  },
  {
    productSummary: {
      description: 'SCH40 ELBOW',
      partNumber: null,
      id: null,
    },
    quantityRequested: 50,
  },
  {
    productSummary: {
      description: 'PVC-3/4',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 50,
  },
  {
    productSummary: {
      description: '3/4 EMT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 100,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 50,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 50,
  },
];
const requestedLineItems3 = [
  {
    productSummary: {
      description: '1 1/4" LB die cast combo',
      partNumber: null,
      id: null,
    },
    quantityRequested: 5320,
  },
  {
    productSummary: {
      description: '1-1/4" UNIVERSAL CLAMP (STRUT STRAP)',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 250,
  },
  {
    productSummary: {
      description: '8" x 6" x 4" jb no ko',
      partNumber: null,
      id: null,
    },
    quantityRequested: 88000,
  },
  {
    productSummary: {
      description: 'STEEL THINWALL CONDUIT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 47500,
  },
  {
    productSummary: {
      description: 'SCH40 COND 10FT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 12500,
  },
  {
    productSummary: {
      description: 'SCH40',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 5000,
  },
  {
    productSummary: {
      description: 'ELL-90-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 4500,
  },
  {
    productSummary: {
      description: 'SCH40 ELBOW',
      partNumber: null,
      id: null,
    },
    quantityRequested: 18000,
  },
  {
    productSummary: {
      description: 'PVC-3/4',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 50,
  },
  {
    productSummary: {
      description: '3/4 EMT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 50,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 200,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 2500,
  },
  {
    productSummary: {
      description: 'SCH40 ELBOW',
      partNumber: null,
      id: null,
    },
    quantityRequested: 475,
  },
  {
    productSummary: {
      description: 'PVC-3/4',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 25,
  },
  {
    productSummary: {
      description: '3/4 EMT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 450,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 1900,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 200,
  },
  {
    productSummary: {
      description: '3/4 EMT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 300,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 35000,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 300,
  },
  {
    productSummary: {
      description: 'SCH40 ELBOW',
      partNumber: null,
      id: null,
    },
    quantityRequested: 1600,
  },
  {
    productSummary: {
      description: 'PVC-3/4',
      partNumber: '703 1 1/4',
      id: 'ced2bff3e9df4d7492567f8c1a7a7817',
    },
    quantityRequested: 120,
  },
  {
    productSummary: {
      description: '3/4 EMT',
      partNumber: null,
      id: null,
    },
    quantityRequested: 110,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 600,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 1000,
  },
  {
    productSummary: {
      description: 'COUP-3/4',
      partNumber: null,
      id: null,
    },
    quantityRequested: 250,
  },
];
const correctAnswer1 = {
  comment: '',
  totalCostInCents: 115136,
  lineItems: [
    {
      productSummary: {
        description: '1 1/4" LB die cast combo',
        partNumber: null,
        id: null,
      },
      quantityRequested: 35,
      quantityAvailable: 35,
      quantityPerGroup: 35,
      pricePerGroupInCents: 5705,
      comment: 'Nonstock Item May Not Be Returnable ',
    },
    {
      productSummary: {
        description: '1-1/4" UNIVERSAL CLAMP (STRUT STRAP)',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 20,
      quantityAvailable: 20,
      quantityPerGroup: 20,
      pricePerGroupInCents: 4700,
    },
    {
      productSummary: {
        description: '8" x 6" x 4" jb no ko',
        partNumber: null,
        id: null,
      },
      quantityRequested: 20,
      quantityAvailable: 20,
      quantityPerGroup: 20,
      pricePerGroupInCents: 560,
    },
    {
      productSummary: {
        description: 'STEEL THINWALL CONDUIT',
        partNumber: null,
        id: null,
      },
      quantityRequested: 15,
      quantityAvailable: 15,
      quantityPerGroup: 15,
      pricePerGroupInCents: 885,
    },
    {
      productSummary: {
        description: 'SCH40 COND 10FT',
        partNumber: null,
        id: null,
      },
      quantityRequested: 25,
      quantityAvailable: 25,
      quantityPerGroup: 25,
      pricePerGroupInCents: 1475,
    },
    {
      productSummary: {
        description: 'SCH40',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 5,
      quantityAvailable: 5,
      quantityPerGroup: 5,
      pricePerGroupInCents: 150,
    },
    {
      productSummary: { description: 'ELL-90-3/4', partNumber: null, id: null },
      quantityRequested: 10,
      quantityAvailable: 10,
      quantityPerGroup: 10,
      pricePerGroupInCents: 2650,
    },
    {
      productSummary: {
        description: 'SCH40 ELBOW',
        partNumber: null,
        id: null,
      },
      quantityRequested: 5,
      quantityAvailable: 5,
      quantityPerGroup: 5,
      pricePerGroupInCents: 2110,
    },
    {
      productSummary: {
        description: 'PVC-3/4',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 3,
      quantityAvailable: 3,
      quantityPerGroup: 3,
      pricePerGroupInCents: 2721,
      comment: 'Nonstock Item May Not Be Returnable ',
    },
    {
      productSummary: { description: '3/4 EMT', partNumber: null, id: null },
      quantityRequested: 100,
      quantityAvailable: 100,
      quantityPerGroup: 100,
      pricePerGroupInCents: 45200,
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 100,
      quantityAvailable: 100,
      quantityPerGroup: 100,
      pricePerGroupInCents: 47600,
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 20,
      quantityAvailable: 20,
      quantityPerGroup: 20,
      pricePerGroupInCents: 1380,
    },
  ],
};
const correctAnswer2 = {
  comment: '',
  totalCostInCents: 1155339,
  lineItems: [
    {
      productSummary: {
        description: '1 1/4" LB die cast combo',
        partNumber: null,
        id: null,
      },
      quantityRequested: 20000,
      quantityAvailable: 20000,
      quantityPerGroup: 20000,
      pricePerGroupInCents: 645833,
    },
    {
      productSummary: {
        description: '1-1/4" UNIVERSAL CLAMP (STRUT STRAP)',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 5000,
      quantityAvailable: 5000,
      quantityPerGroup: 5000,
      pricePerGroupInCents: 261250,
    },
    {
      productSummary: {
        description: '8" x 6" x 4" jb no ko',
        partNumber: null,
        id: null,
      },
      quantityRequested: 5000,
      quantityAvailable: 5000,
      quantityPerGroup: 5000,
      pricePerGroupInCents: 82730,
      comment: "2500' REEL ",
    },
    {
      productSummary: {
        description: 'STEEL THINWALL CONDUIT',
        partNumber: null,
        id: null,
      },
      quantityRequested: 5000,
      quantityAvailable: 5000,
      quantityPerGroup: 5000,
      pricePerGroupInCents: 82730,
      comment: "2500' REEL ",
    },
    {
      productSummary: {
        description: 'SCH40 COND 10FT',
        partNumber: null,
        id: null,
      },
      quantityRequested: 100,
      quantityAvailable: 100,
      quantityPerGroup: 100,
      pricePerGroupInCents: 7172,
    },
    {
      productSummary: {
        description: 'SCH40',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 200,
      quantityAvailable: 200,
      quantityPerGroup: 200,
      pricePerGroupInCents: 10709,
      comment: '5503-45-00 ',
    },
    {
      productSummary: { description: 'ELL-90-3/4', partNumber: null, id: null },
      quantityRequested: 50,
      quantityAvailable: 50,
      quantityPerGroup: 50,
      pricePerGroupInCents: 2060,
    },
    {
      productSummary: {
        description: 'SCH40 ELBOW',
        partNumber: null,
        id: null,
      },
      quantityRequested: 50,
      quantityAvailable: 50,
      quantityPerGroup: 50,
      pricePerGroupInCents: 4189,
    },
    {
      productSummary: {
        description: 'PVC-3/4',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 200,
      quantityAvailable: 200,
      quantityPerGroup: 200,
      pricePerGroupInCents: 7707,
      comment: '5502-30-00 ',
    },
    {
      productSummary: { description: '3/4 EMT', partNumber: null, id: null },
      quantityRequested: 50,
      quantityAvailable: 50,
      quantityPerGroup: 50,
      pricePerGroupInCents: 1052,
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 50,
      quantityAvailable: 50,
      quantityPerGroup: 50,
      pricePerGroupInCents: 1618,
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 100,
      quantityAvailable: 100,
      quantityPerGroup: 100,
      pricePerGroupInCents: 6116,
    },
    {
      productSummary: {
        description: 'SCH40 ELBOW',
        partNumber: null,
        id: null,
      },
      quantityRequested: 50,
      quantityAvailable: 50,
      quantityPerGroup: 50,
      pricePerGroupInCents: 8859,
    },
    {
      productSummary: {
        description: 'PVC-3/4',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 50,
      quantityAvailable: 50,
      quantityPerGroup: 50,
      pricePerGroupInCents: 14155,
    },
    {
      productSummary: { description: '3/4 EMT', partNumber: null, id: null },
      quantityRequested: 100,
      quantityAvailable: 100,
      quantityPerGroup: 100,
      pricePerGroupInCents: 4649,
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 50,
      quantityAvailable: 50,
      quantityPerGroup: 50,
      pricePerGroupInCents: 5497,
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 50,
      quantityAvailable: 50,
      quantityPerGroup: 50,
      pricePerGroupInCents: 9013,
    },
  ],
};
const correctAnswer3 = {
  totalCostInCents: 5759662,
  comment: '',
  lineItems: [
    {
      productSummary: {
        description: '1 1/4" LB die cast combo',
        partNumber: null,
        id: null,
      },
      quantityRequested: 5320,
      quantityAvailable: 5320,
      quantityPerGroup: 5320,
      pricePerGroupInCents: 250040,
    },
    {
      productSummary: {
        description: '1-1/4" UNIVERSAL CLAMP (STRUT STRAP)',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 250,
      quantityAvailable: 250,
      quantityPerGroup: 250,
      pricePerGroupInCents: 4525,
    },
    {
      productSummary: {
        description: '8" x 6" x 4" jb no ko',
        partNumber: null,
        id: null,
      },
      quantityRequested: 88000,
      quantityAvailable: 88000,
      quantityPerGroup: 88000,
      pricePerGroupInCents: 1601600,
    },
    {
      productSummary: {
        description: 'STEEL THINWALL CONDUIT',
        partNumber: null,
        id: null,
      },
      quantityRequested: 47500,
      quantityAvailable: 47500,
      quantityPerGroup: 47500,
      pricePerGroupInCents: 1273000,
    },
    {
      productSummary: {
        description: 'SCH40 COND 10FT',
        partNumber: null,
        id: null,
      },
      quantityRequested: 12500,
      quantityAvailable: 12500,
      quantityPerGroup: 12500,
      pricePerGroupInCents: 336250,
    },
    {
      productSummary: {
        description: 'SCH40',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 5000,
      quantityAvailable: 5000,
      quantityPerGroup: 5000,
      pricePerGroupInCents: 304000,
    },
    {
      productSummary: { description: 'ELL-90-3/4', partNumber: null, id: null },
      quantityRequested: 4500,
      quantityAvailable: 4500,
      quantityPerGroup: 4500,
      pricePerGroupInCents: 292207,
    },
    {
      productSummary: {
        description: 'SCH40 ELBOW',
        partNumber: null,
        id: null,
      },
      quantityRequested: 18000,
      quantityAvailable: 18000,
      quantityPerGroup: 18000,
      pricePerGroupInCents: 79200,
    },
    {
      productSummary: {
        description: 'PVC-3/4',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 50,
      quantityAvailable: 50,
      quantityPerGroup: 50,
      pricePerGroupInCents: 5900,
      comment: '1/2" & 3/4" KOS ',
    },
    {
      productSummary: { description: '3/4 EMT', partNumber: null, id: null },
      quantityRequested: 50,
      quantityAvailable: 50,
      quantityPerGroup: 50,
      pricePerGroupInCents: 10600,
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 200,
      quantityAvailable: 200,
      quantityPerGroup: 200,
      pricePerGroupInCents: 42000,
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 2500,
      quantityAvailable: 2500,
      quantityPerGroup: 2500,
      pricePerGroupInCents: 415000,
    },
    {
      productSummary: {
        description: 'SCH40 ELBOW',
        partNumber: null,
        id: null,
      },
      quantityRequested: 475,
      quantityAvailable: 475,
      quantityPerGroup: 475,
      pricePerGroupInCents: 207100,
    },
    {
      productSummary: {
        description: 'PVC-3/4',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 25,
      quantityAvailable: 25,
      quantityPerGroup: 25,
      pricePerGroupInCents: 14150,
    },
    {
      productSummary: { description: '3/4 EMT', partNumber: null, id: null },
      quantityRequested: 450,
      quantityAvailable: 450,
      quantityPerGroup: 450,
      pricePerGroupInCents: 67950,
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 1900,
      quantityAvailable: 1900,
      quantityPerGroup: 1900,
      pricePerGroupInCents: 640300,
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 200,
      quantityAvailable: 200,
      quantityPerGroup: 200,
      pricePerGroupInCents: 6200,
    },
    {
      productSummary: { description: '3/4 EMT', partNumber: null, id: null },
      quantityRequested: 300,
      quantityAvailable: 300,
      quantityPerGroup: 300,
      pricePerGroupInCents: 51900,
      comment: '.5 OFST BEIGE ',
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 35000,
      quantityAvailable: 35000,
      quantityPerGroup: 35000,
      pricePerGroupInCents: 60900,
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 300,
      quantityAvailable: 300,
      quantityPerGroup: 300,
      pricePerGroupInCents: 1530,
    },
    {
      productSummary: {
        description: 'SCH40 ELBOW',
        partNumber: null,
        id: null,
      },
      quantityRequested: 1600,
      quantityAvailable: 1600,
      quantityPerGroup: 1600,
      pricePerGroupInCents: 33600,
    },
    {
      productSummary: {
        description: 'PVC-3/4',
        partNumber: '703 1 1/4',
        id: 'ced2bff3e9df4d7492567f8c1a7a7817',
      },
      quantityRequested: 120,
      quantityAvailable: 120,
      quantityPerGroup: 120,
      pricePerGroupInCents: 4320,
    },
    {
      productSummary: { description: '3/4 EMT', partNumber: null, id: null },
      quantityRequested: 110,
      quantityAvailable: 110,
      quantityPerGroup: 110,
      pricePerGroupInCents: 5390,
      comment: 'Nonstock Item May Not Be Returnable ',
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 600,
      quantityAvailable: 600,
      quantityPerGroup: 600,
      pricePerGroupInCents: 30000,
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 1000,
      quantityAvailable: 1000,
      quantityPerGroup: 1000,
      pricePerGroupInCents: 9500,
    },
    {
      productSummary: { description: 'COUP-3/4', partNumber: null, id: null },
      quantityRequested: 250,
      quantityAvailable: 250,
      quantityPerGroup: 250,
      pricePerGroupInCents: 12500,
    },
  ],
};

function calculateTotalCostInCentsForLineItems(lineItems) {
  let actualTotal = 0;
  lineItems.forEach((li) => {
    actualTotal += li.pricePerGroupInCents;
  });
  return actualTotal;
}

/* eslint-disable no-undef */
describe('Hz PDF 1 with equal number of requested lines', () => {
  it('updated line items match answer 1', () => {
    const pdfReadStream1 = fs.createReadStream('./suppliers/hz/hz1.pdf');
    const result = generateQuoteLineItemsFromHzExport(
      pdfReadStream1,
      requestedLineItems1,
    );
    return result.then((fulfilled) => {
      expect(fulfilled.comment).to.equal(correctAnswer1.comment);
      fulfilled.lineItems.forEach((part, index) => {
        expect(part).to.deep.equal(correctAnswer1.lineItems[index]);
      });
    });
  });
});

describe('HZ PDF 1 with incorrect number of requested lines', () => {
  it('should error', () => {
    const pdfReadStream1 = fs.createReadStream('./suppliers/hz/hz1.pdf');
    const result = generateQuoteLineItemsFromHzExport(
      pdfReadStream1,
      requestedLineItems2,
    );
    return expect(result).to.be.rejectedWith(
      'Incorrect Number of items requested',
    );
  });
});

describe('HZ PDF 2 with equal number of requested lines', () => {
  it('updated line items match answer 2', () => {
    const pdfReadStream2 = fs.createReadStream('./suppliers/hz/hz2.pdf');
    const result2 = generateQuoteLineItemsFromHzExport(
      pdfReadStream2,
      requestedLineItems2,
    );
    return result2.then((fulfilled) => {
      expect(fulfilled.comment).to.equal(correctAnswer2.comment);
      expect(
        calculateTotalCostInCentsForLineItems(fulfilled.lineItems),
      ).to.equal(correctAnswer2.totalCostInCents);
      fulfilled.lineItems.forEach((part, index) => {
        expect(part).to.deep.equal(correctAnswer2.lineItems[index]);
      });
    });
  });
});

describe('HZ PDF 3 with equal number of requested lines', () => {
  it('updated line items match answer 3', () => {
    const pdfReadStream3 = fs.createReadStream('./suppliers/hz/hz3.pdf');
    const result3 = generateQuoteLineItemsFromHzExport(
      pdfReadStream3,
      requestedLineItems3,
    );
    return result3.then((fulfilled) => {
      expect(fulfilled.comment).to.equal(correctAnswer3.comment);
      expect(
        calculateTotalCostInCentsForLineItems(fulfilled.lineItems),
      ).to.equal(correctAnswer3.totalCostInCents);
      fulfilled.lineItems.forEach((part, index) => {
        expect(part).to.deep.equal(correctAnswer3.lineItems[index]);
      });
    });
  });
});
