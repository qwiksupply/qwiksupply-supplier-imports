const pdfreader = require('pdfreader');
const fs = require('fs');
const tmp = require('tmp');

const DESCRIPTION_X1 = 23.916;
const DESCRIPTION_X2 = 23.923;
const EXTPRICE_X = 89.023;
const QUANTITY_X_LOWERBOUND = 14;
const QUANTITY_X_HIGHERBOUND = 19;
const UNIT_PRICE_X_LOWERBOUND = 73;
const UNIT_PRICE_X_HIGHERBOUND = 88;
const NUM_LINES_BETWEEN_PAGES = 60;
const LINES_PER_OBJ = 8;

function generateQuoteLineItemsFromHzExport(exportStream, requestedLineItems) {
  const tmpobj = tmp.fileSync();
  const pdfName = tmpobj.name;
  return new Promise((resolve, reject) => {
    const pdfWriteStream = fs.createWriteStream(pdfName);
    pdfWriteStream.on('close', () => {
      const pageContent = [];
      const lineItemDataFromPDF = { headerNote: '', lineItems: [] };
      new pdfreader.PdfReader().parseFileItems(pdfName, (err, item) => {
        const hasReachedEndOfPage = !item;
        if (hasReachedEndOfPage) {
          getLineItemsFromPDF(lineItemDataFromPDF, pageContent);
          if (hasCorrectNumberOfLineItems(requestedLineItems, lineItemDataFromPDF)) {
            if (hasValidQuotedQuantities(lineItemDataFromPDF, requestedLineItems)) {
              const updatedLineItems =
              generateUpdatedLineItems(lineItemDataFromPDF, requestedLineItems);
              const headerItemObject = { comment: '', lineItems: updatedLineItems };
              resolve(headerItemObject);
            } else {
              reject(new Error('Invalid Quantities'));
            }
          } else {
            reject(new Error('Incorrect Number of items requested'));
          }
        } else if (item.text) {
          pageContent.push(item);
        }
      });
    });
    exportStream.pipe(pdfWriteStream);
  });
}

function hasValidQuotedQuantities(lineItemDataFromPDF, requestedLineItems) {
  let foundNeg = true;
  lineItemDataFromPDF.lineItems.forEach((item, index) => {
    if (item.quantity < 0 || item.quantity > requestedLineItems[index].quantityRequested) {
      foundNeg = false;
    }
  });
  return foundNeg;
}

function hasCorrectNumberOfLineItems(requestedLineItems, lineItemDataFromPDF) {
  return lineItemDataFromPDF.lineItems.length === requestedLineItems.length;
}

function getLineItemsFromPDF(lineItemDataFromPDF, pageContent) {
  const productStartLineNums = getProductStartLineNums(pageContent);
  productStartLineNums.forEach((startLineNumber, indexOfProduct) => {
    let nextPartStarts = productStartLineNums[indexOfProduct + 1];
    if (indexOfProduct === (productStartLineNums.length - 1)) {
      nextPartStarts = pageContent.length;
    }
    if ((nextPartStarts - startLineNumber) >= NUM_LINES_BETWEEN_PAGES) {
      nextPartStarts = startLineNumber + LINES_PER_OBJ;
    }
    const product = pageContent.slice(startLineNumber, nextPartStarts);
    const productObj = getProductData(product);
    if (lineItemDataFromPDF.lineItems.every(onlyAppearsOnce, productObj)) {
      lineItemDataFromPDF.lineItems.push(productObj);
    }
  });
}

function getProductStartLineNums(pageContent) {
  const productStartLineNums = [];
  pageContent.forEach((line, index) => {
    if (line.text.includes('ea') || line.text.includes('ft')) {
      if ((line.x) > QUANTITY_X_LOWERBOUND && (line.x) < QUANTITY_X_HIGHERBOUND) {
        productStartLineNums.push(index - 1);
      }
    }
  });
  return productStartLineNums;
}

function getProductData(product) {
  let quantity = 0;
  let description = '';
  let extendedPrice = '';
  let unitPrice = '';
  let comment = '';
  product.forEach((line) => {
    const lineText = line.text;
    const xcoord = line.x;
    if (xcoord < QUANTITY_X_HIGHERBOUND && xcoord > QUANTITY_X_LOWERBOUND) {
      quantity = parseInt(lineText, 10);
    }
    if (xcoord >= DESCRIPTION_X1 && xcoord <= DESCRIPTION_X2) {
      if (lineText[1] === lineText[1].toLowerCase() && (!lineText.includes('ea '))) {
        comment = `${comment}${lineText} `;
      } else {
        description = `${description}${lineText} `;
      }
    }
    if (xcoord > EXTPRICE_X) {
      extendedPrice = parseInt(lineText.replace('.', ''), 10);
    }
    if (xcoord < UNIT_PRICE_X_HIGHERBOUND && xcoord > UNIT_PRICE_X_LOWERBOUND) {
      unitPrice = parseInt(lineText.replace('.', ''), 10) / 1000;
    }
  });
  description = description.trim();
  return { description, quantity, extendedPrice, unitPrice, comment };
}

function onlyAppearsOnce(element) {
  return element.description !== this.description;
}


function generateUpdatedLineItems(lineItemDataFromPDF, requestedLineItems) {
  const updatedLineItems = requestedLineItems.map((requestedLineItem, index) => {
    const updatedLineItem = {
      productSummary: requestedLineItem.productSummary,
      quantityRequested: requestedLineItem.quantityRequested,
      quantityAvailable: lineItemDataFromPDF.lineItems[index].quantity,
      quantityPerGroup: lineItemDataFromPDF.lineItems[index].quantity,
      pricePerGroupInCents: lineItemDataFromPDF.lineItems[index].extendedPrice,
    };
    if (lineItemDataFromPDF.lineItems[index].comment !== '') {
      updatedLineItem.comment = lineItemDataFromPDF.lineItems[index].comment;
    }
    return updatedLineItem;
  });
  return updatedLineItems;
}

module.exports = generateQuoteLineItemsFromHzExport;
