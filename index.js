const generateQuoteLineItemsFromHzExport = require('./suppliers/hz/parse-hz-pdfs.js');
const fxns = require('./suppliers/graybar/parse-graybar-pdfs.js');

const generateQuoteLineItemsFromGraybarExport = fxns.generateLineItems;

module.exports = { generateQuoteLineItemsFromGraybarExport, generateQuoteLineItemsFromHzExport };
