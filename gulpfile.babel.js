import gulp from 'gulp';
import babel from 'gulp-babel';
import eslint from 'gulp-eslint';
import mocha from 'gulp-mocha';

const paths = {
  indexJs: 'index.js',
  parserJs: '**/parse-*.js',
  supplierCode: 'suppliers/**/*.js',
  allTests: 'suppliers/**/*-test.js',
  distDir: 'dist',
  exlcudeDist: '!dist/**/*',
  excludeNodeModules: '!node_modules/**/*',
};

gulp.task('build', ['test'], () =>
  gulp.src([paths.indexJs, paths.parserJs, paths.exlcudeDist, paths.excludeNodeModules])
    .pipe(babel())
    .pipe(gulp.dest(paths.distDir)),
);

gulp.task('test', ['lint'], () =>
  gulp.src(paths.allTests)
    .pipe(babel())
    .pipe(mocha({ reporter: 'nyan' })),
);

gulp.task('lint', () =>
  gulp.src([paths.indexJs, paths.supplierCode])
    .pipe(eslint())
    .pipe(eslint.format()),
);
