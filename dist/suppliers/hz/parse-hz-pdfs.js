'use strict';

var pdfreader = require('pdfreader');
var fs = require('fs');
var tmp = require('tmp');

var DESCRIPTION_X1 = 23.916;
var DESCRIPTION_X2 = 23.923;
var EXTPRICE_X = 89.023;
var QUANTITY_X_LOWERBOUND = 14;
var QUANTITY_X_HIGHERBOUND = 19;
var UNIT_PRICE_X_LOWERBOUND = 73;
var UNIT_PRICE_X_HIGHERBOUND = 88;
var NUM_LINES_BETWEEN_PAGES = 60;
var LINES_PER_OBJ = 8;

function generateQuoteLineItemsFromHzExport(exportStream, requestedLineItems) {
  var tmpobj = tmp.fileSync();
  var pdfName = tmpobj.name;
  return new Promise(function (resolve, reject) {
    var pdfWriteStream = fs.createWriteStream(pdfName);
    pdfWriteStream.on('close', function () {
      var pageContent = [];
      var lineItemDataFromPDF = { headerNote: '', lineItems: [] };
      new pdfreader.PdfReader().parseFileItems(pdfName, function (err, item) {
        var hasReachedEndOfPage = !item;
        if (hasReachedEndOfPage) {
          getLineItemsFromPDF(lineItemDataFromPDF, pageContent);
          if (hasCorrectNumberOfLineItems(requestedLineItems, lineItemDataFromPDF)) {
            if (hasValidQuotedQuantities(lineItemDataFromPDF, requestedLineItems)) {
              var updatedLineItems = generateUpdatedLineItems(lineItemDataFromPDF, requestedLineItems);
              var headerItemObject = { comment: '', lineItems: updatedLineItems };
              resolve(headerItemObject);
            } else {
              reject(new Error('Invalid Quantities'));
            }
          } else {
            reject(new Error('Incorrect Number of items requested'));
          }
        } else if (item.text) {
          pageContent.push(item);
        }
      });
    });
    exportStream.pipe(pdfWriteStream);
  });
}

function hasValidQuotedQuantities(lineItemDataFromPDF, requestedLineItems) {
  var foundNeg = true;
  lineItemDataFromPDF.lineItems.forEach(function (item, index) {
    if (item.quantity < 0 || item.quantity > requestedLineItems[index].quantityRequested) {
      foundNeg = false;
    }
  });
  return foundNeg;
}

function hasCorrectNumberOfLineItems(requestedLineItems, lineItemDataFromPDF) {
  return lineItemDataFromPDF.lineItems.length === requestedLineItems.length;
}

function getLineItemsFromPDF(lineItemDataFromPDF, pageContent) {
  var productStartLineNums = getProductStartLineNums(pageContent);
  productStartLineNums.forEach(function (startLineNumber, indexOfProduct) {
    var nextPartStarts = productStartLineNums[indexOfProduct + 1];
    if (indexOfProduct === productStartLineNums.length - 1) {
      nextPartStarts = pageContent.length;
    }
    if (nextPartStarts - startLineNumber >= NUM_LINES_BETWEEN_PAGES) {
      nextPartStarts = startLineNumber + LINES_PER_OBJ;
    }
    var product = pageContent.slice(startLineNumber, nextPartStarts);
    var productObj = getProductData(product);
    if (lineItemDataFromPDF.lineItems.every(onlyAppearsOnce, productObj)) {
      lineItemDataFromPDF.lineItems.push(productObj);
    }
  });
}

function getProductStartLineNums(pageContent) {
  var productStartLineNums = [];
  pageContent.forEach(function (line, index) {
    if (line.text.includes('ea') || line.text.includes('ft')) {
      if (line.x > QUANTITY_X_LOWERBOUND && line.x < QUANTITY_X_HIGHERBOUND) {
        productStartLineNums.push(index - 1);
      }
    }
  });
  return productStartLineNums;
}

function getProductData(product) {
  var quantity = 0;
  var description = '';
  var extendedPrice = '';
  var unitPrice = '';
  var comment = '';
  product.forEach(function (line) {
    var lineText = line.text;
    var xcoord = line.x;
    if (xcoord < QUANTITY_X_HIGHERBOUND && xcoord > QUANTITY_X_LOWERBOUND) {
      quantity = parseInt(lineText, 10);
    }
    if (xcoord >= DESCRIPTION_X1 && xcoord <= DESCRIPTION_X2) {
      if (lineText[1] === lineText[1].toLowerCase() && !lineText.includes('ea ')) {
        comment = '' + comment + lineText + ' ';
      } else {
        description = '' + description + lineText + ' ';
      }
    }
    if (xcoord > EXTPRICE_X) {
      extendedPrice = parseInt(lineText.replace('.', ''), 10);
    }
    if (xcoord < UNIT_PRICE_X_HIGHERBOUND && xcoord > UNIT_PRICE_X_LOWERBOUND) {
      unitPrice = parseInt(lineText.replace('.', ''), 10) / 1000;
    }
  });
  description = description.trim();
  return { description: description, quantity: quantity, extendedPrice: extendedPrice, unitPrice: unitPrice, comment: comment };
}

function onlyAppearsOnce(element) {
  return element.description !== this.description;
}

function generateUpdatedLineItems(lineItemDataFromPDF, requestedLineItems) {
  var updatedLineItems = requestedLineItems.map(function (requestedLineItem, index) {
    var updatedLineItem = {
      productSummary: requestedLineItem.productSummary,
      quantityRequested: requestedLineItem.quantityRequested,
      quantityAvailable: lineItemDataFromPDF.lineItems[index].quantity,
      quantityPerGroup: lineItemDataFromPDF.lineItems[index].quantity,
      pricePerGroupInCents: lineItemDataFromPDF.lineItems[index].extendedPrice
    };
    if (lineItemDataFromPDF.lineItems[index].comment !== '') {
      updatedLineItem.comment = lineItemDataFromPDF.lineItems[index].comment;
    }
    return updatedLineItem;
  });
  return updatedLineItems;
}

module.exports = generateQuoteLineItemsFromHzExport;