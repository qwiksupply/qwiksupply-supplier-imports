'use strict';

var pdfreader = require('pdfreader');
var fs = require('fs');
var tmp = require('tmp');

var PARTNUM_X = 33.859;
var DESCRIPTION_X = 51.888;
var EXTPRICE_X = 90;
var UNIT_X = 78.684;
var QUANTITY_X_LOWERBOUND = 12;
var QUANTITY_X_HIGHERBOUND = 14;
var FIRST_PART = '100';
var SKIP_LINES = 2;
var NUM_LAST_PRODUCT_LINES = 13;

function generateQuoteLineItemsFromGraybarExport(exportStream, requestedLineItems) {
  var tmpobj = tmp.fileSync();
  var pdfName = tmpobj.name;
  return new Promise(function (resolve, reject) {
    var pdfWriteStream = fs.createWriteStream(pdfName);
    pdfWriteStream.on('close', function () {
      var pageContent = [];
      var lineItemDataFromPDF = { headerNote: '', lineItems: [] };
      new pdfreader.PdfReader().parseFileItems(pdfName, function (err, item) {
        var hasReachedEndOfPage = !item;
        if (hasReachedEndOfPage) {
          if (hasCorrectNumberOfLineItems(requestedLineItems, lineItemDataFromPDF)) {
            if (hasValidQuotedQuantities(lineItemDataFromPDF, requestedLineItems)) {
              var updatedLineItems = generateUpdatedLineItems(lineItemDataFromPDF, requestedLineItems);
              var headerItemObject = { comment: lineItemDataFromPDF.headerNote, lineItems: updatedLineItems };
              resolve(headerItemObject);
            } else {
              reject(new Error('Invalid Quantities'));
            }
          } else {
            reject(new Error('Incorrect Number of items requested'));
          }
        } else if (item.text) {
          pageContent.push(item);
        } else if (item.page) {
          lineItemDataFromPDF = getLineItemsFromPDF(lineItemDataFromPDF, pageContent);
          pageContent = [];
        }
      });
    });
    exportStream.pipe(pdfWriteStream);
  });
}

function hasValidQuotedQuantities(lineItemDataFromPDF, requestedLineItems) {
  var foundNeg = true;
  lineItemDataFromPDF.lineItems.forEach(function (item, index) {
    if (item.quantity < 0 || item.quantity > requestedLineItems[index].quantityRequested) {
      foundNeg = false;
    }
  });
  return foundNeg;
}

function hasCorrectNumberOfLineItems(requestedLineItems, lineItemDataFromPDF) {
  return lineItemDataFromPDF.lineItems.length === requestedLineItems.length;
}

function getLineItemsFromPDF(lineItemDataFromPDF, pageContent) {
  var updatedPDFData = { headerNote: '', lineItems: [] };
  var productStartLineNums = getProductStartLineNums(pageContent);
  updatedPDFData.headerNote = getHeader(pageContent);
  updatedPDFData.lineItems = lineItemDataFromPDF.lineItems;
  productStartLineNums.forEach(function (startLineNumber, indexOfProduct) {
    var nextPartStarts = productStartLineNums[indexOfProduct + 1];
    if (indexOfProduct === productStartLineNums.length - 1) {
      nextPartStarts = startLineNumber + NUM_LAST_PRODUCT_LINES;
    }
    var product = pageContent.slice(startLineNumber, nextPartStarts - 1);
    var productObj = getProductData(product);
    if (updatedPDFData.lineItems.every(onlyAppearsOnce, productObj)) {
      updatedPDFData.lineItems.push(productObj);
    }
  });
  return updatedPDFData;
}

function getHeader(pageContent) {
  var header = '';
  var indexOfHeaderStart = pageContent.findIndex(function (line) {
    return line.text.includes('Ext.Price');
  }) + SKIP_LINES;
  var indexOfFirstPart = pageContent.findIndex(function (line) {
    return line.text.includes(FIRST_PART);
  });
  var headerSlice = pageContent.slice(indexOfHeaderStart, indexOfFirstPart);
  var headerText = [];
  headerSlice.forEach(function (line) {
    return headerText.push(line.text);
  });
  header = headerText.join(' ').replace('Notes: ', '');
  if (header.includes('EA ')) {
    header = '';
  }
  return header;
}

function getProductStartLineNums(pageContent) {
  var productStartLineNums = [];
  pageContent.forEach(function (line, index) {
    if (line.text.includes('EA')) {
      if (line.x > QUANTITY_X_LOWERBOUND && line.x < QUANTITY_X_HIGHERBOUND) {
        productStartLineNums.push(index);
      }
    }
  });
  return productStartLineNums;
}

function getProductData(product) {
  var quantity = 0;
  var partNumber = '';
  var description = '';
  var extendedPrice = '';
  var comment = '';
  var unit = '';
  var noteStarted = false;
  product.forEach(function (line, indexOfLine) {
    var lineText = line.text;
    var xcoord = line.x;
    if (indexOfLine === 0) {
      quantity = parseInt(lineText, 10);
    }
    if (xcoord === PARTNUM_X) {
      partNumber = '' + partNumber + lineText + ' ';
    }
    if (xcoord === DESCRIPTION_X) {
      description = '' + description + lineText + ' ';
    }
    if (xcoord > EXTPRICE_X) {
      extendedPrice = parseInt(lineText.replace('$', '').replace('.', ''), 10);
    }
    if (xcoord === UNIT_X) {
      unit = parseInt(lineText, 10);
    }
    if (lineText.includes('***Item Note:') || noteStarted) {
      comment += lineText.replace('***Item Note:***', '').replace(/_/g, '');
      noteStarted = true;
    }
  });
  partNumber = partNumber.trim();
  description = description.trim();
  return { partNumber: partNumber, description: description, quantity: quantity, extendedPrice: extendedPrice, comment: comment, unit: unit };
}

function onlyAppearsOnce(element) {
  return element.description !== this.description;
}

function generateUpdatedLineItems(lineItemDataFromPDF, requestedLineItems) {
  var updatedLineItems = requestedLineItems.map(function (requestedLineItem, index) {
    var updatedLineItem = {
      productSummary: requestedLineItem.productSummary,
      quantityRequested: requestedLineItem.quantityRequested,
      quantityAvailable: lineItemDataFromPDF.lineItems[index].quantity,
      quantityPerGroup: lineItemDataFromPDF.lineItems[index].quantity,
      pricePerGroupInCents: lineItemDataFromPDF.lineItems[index].extendedPrice
    };
    if (lineItemDataFromPDF.lineItems[index].comment !== '') {
      updatedLineItem.comment = lineItemDataFromPDF.lineItems[index].comment;
    }
    return updatedLineItem;
  });
  return updatedLineItems;
}

module.exports = { generateLineItems: generateQuoteLineItemsFromGraybarExport,
  checkQuantities: hasValidQuotedQuantities };