'use strict';

var generateQuoteLineItemsFromHzExport = require('./suppliers/hz/parse-hz-pdfs.js');
var fxns = require('./suppliers/graybar/parse-graybar-pdfs.js');

var generateQuoteLineItemsFromGraybarExport = fxns.generateLineItems;

module.exports = { generateQuoteLineItemsFromGraybarExport: generateQuoteLineItemsFromGraybarExport, generateQuoteLineItemsFromHzExport: generateQuoteLineItemsFromHzExport };